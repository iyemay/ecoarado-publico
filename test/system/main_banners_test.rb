require "application_system_test_case"

class MainBannersTest < ApplicationSystemTestCase
  setup do
    @main_banner = main_banners(:one)
  end

  test "visiting the index" do
    visit main_banners_url
    assert_selector "h1", text: "Main Banners"
  end

  test "creating a Main banner" do
    visit main_banners_url
    click_on "New Main Banner"

    fill_in "Order", with: @main_banner.order
    fill_in "Title", with: @main_banner.title
    click_on "Create Main banner"

    assert_text "Main banner was successfully created"
    click_on "Back"
  end

  test "updating a Main banner" do
    visit main_banners_url
    click_on "Edit", match: :first

    fill_in "Order", with: @main_banner.order
    fill_in "Title", with: @main_banner.title
    click_on "Update Main banner"

    assert_text "Main banner was successfully updated"
    click_on "Back"
  end

  test "destroying a Main banner" do
    visit main_banners_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Main banner was successfully destroyed"
  end
end
