require 'test_helper'

class MainControllerTest < ActionDispatch::IntegrationTest
  test "should get home" do
    get main_home_url
    assert_response :success
  end

  test "should get privacy_policy" do
    get main_privacy_policy_url
    assert_response :success
  end

  test "should get terms_and_conditions" do
    get main_terms_and_conditions_url
    assert_response :success
  end

  test "should get frequent_questions" do
    get main_frequent_questions_url
    assert_response :success
  end

  test "should get who_we_are" do
    get main_who_we_are_url
    assert_response :success
  end

end
