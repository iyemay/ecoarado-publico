require 'test_helper'

class MainBannersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @main_banner = main_banners(:one)
  end

  test "should get index" do
    get main_banners_url
    assert_response :success
  end

  test "should get new" do
    get new_main_banner_url
    assert_response :success
  end

  test "should create main_banner" do
    assert_difference('MainBanner.count') do
      post main_banners_url, params: { main_banner: { order: @main_banner.order, title: @main_banner.title } }
    end

    assert_redirected_to main_banner_url(MainBanner.last)
  end

  test "should show main_banner" do
    get main_banner_url(@main_banner)
    assert_response :success
  end

  test "should get edit" do
    get edit_main_banner_url(@main_banner)
    assert_response :success
  end

  test "should update main_banner" do
    patch main_banner_url(@main_banner), params: { main_banner: { order: @main_banner.order, title: @main_banner.title } }
    assert_redirected_to main_banner_url(@main_banner)
  end

  test "should destroy main_banner" do
    assert_difference('MainBanner.count', -1) do
      delete main_banner_url(@main_banner)
    end

    assert_redirected_to main_banners_url
  end
end
