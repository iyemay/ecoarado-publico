class CreateMainBanners < ActiveRecord::Migration[6.0]
  def change
    create_table :main_banners do |t|
      t.string :title
      t.integer :order

      t.timestamps
    end
  end
end
