class MainController < ApplicationController
  def home
  end

  def privacy_policy
    #@legal_content = '# Hello Markdown'
  end

  def terms_and_conditions
  end

  def frequent_questions
  end

  def who_we_are
  end

  def contact_us
  end

  def model_ea2
  end
end
