class MainBannersController < ApplicationController
  before_action :authorize
  before_action :set_main_banner, only: [:show, :edit, :update, :destroy]

  # GET /main_banners
  # GET /main_banners.json
  def index
    @main_banners = MainBanner.all
  end

  # GET /main_banners/1
  # GET /main_banners/1.json
  def show
  end

  # GET /main_banners/new
  def new
    @main_banner = MainBanner.new
  end

  # GET /main_banners/1/edit
  def edit
  end

  # POST /main_banners
  # POST /main_banners.json
  def create
    @main_banner = MainBanner.new(main_banner_params)

    respond_to do |format|
      if @main_banner.save
        format.html { redirect_to @main_banner, notice: 'Main banner was successfully created.' }
        format.json { render :show, status: :created, location: @main_banner }
      else
        format.html { render :new }
        format.json { render json: @main_banner.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /main_banners/1
  # PATCH/PUT /main_banners/1.json
  def update
    respond_to do |format|
      if @main_banner.update(main_banner_params)
        format.html { redirect_to @main_banner, notice: 'Main banner was successfully updated.' }
        format.json { render :show, status: :ok, location: @main_banner }
      else
        format.html { render :edit }
        format.json { render json: @main_banner.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /main_banners/1
  # DELETE /main_banners/1.json
  def destroy
    @main_banner.destroy
    respond_to do |format|
      format.html { redirect_to main_banners_url, notice: 'Main banner was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_main_banner
      @main_banner = MainBanner.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def main_banner_params
      params.require(:main_banner).permit(:title, :order, :photo)
    end
end
