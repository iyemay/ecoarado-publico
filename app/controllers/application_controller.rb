class ApplicationController < ActionController::Base

  helper_method :admin_page?
  helper_method :authorize
  helper_method :username

  protected

  def admin_page?
    (request.original_fullpath.include? 'admin') || (request.original_fullpath.include? 'login')
  end

  def authorize
    if session[:user_id].nil?
      flash[:warning] = 'Por favor, primero identifíquese.'
      redirect_to login_form_path and return
    end
  end

  def username
    session[:username]
  end
end
