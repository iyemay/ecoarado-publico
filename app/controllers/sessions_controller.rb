class SessionsController < ApplicationController
  def new; end

  def create
    user = User.find_by(name: params[:username].downcase)

    if user&.authenticate(params[:password])
      session[:user_id] = user.id
      session[:username] = user.name

      flash[:notice] = 'Bienvenido ' + user.name
      redirect_to dashboard_path
    else
      flash[:error] = 'Usuario o contraseña incorrecto, por favor intente de nuevo'
      redirect_to login_form_path
    end
  end

  def destroy
    session[:user_id] = nil
    session[:username] = nil

    flash[:notice] = 'Hasta pronto!'
    redirect_to login_form_path
  end
end
