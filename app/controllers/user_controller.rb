class UserController < ApplicationController
  before_action :authorize

  def index
  end

  def show
  end

  def new
  end

  def create
  end

  def destroy
  end
end
