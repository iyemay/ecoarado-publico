json.extract! main_banner, :id, :title, :order, :created_at, :updated_at
json.url main_banner_url(main_banner, format: :json)
