#!/bin/bash

echo "Putting down containers to avoid conflicts"
docker-compose down
docker stop pg-container
docker rm pg-container

docker run --name pg-container \
  -e POSTGRES_PASSWORD=g38Lhc3Dj9PMVyrFjpCxtPveEXe7epYT4whCcGXQW4BCRt2VAqQf4c5h5sJPbDAD \
  -p 5432:5432  \
  -v db_data:/var/lib/postgresql/data  \
  -d postgres:12.2

source .env
bundle exec rails db:reset
bundle exec rails db:setup
bundle exec rails db:migrate
bundle exec rails db:seed
bundle exec rails s -p 3000
