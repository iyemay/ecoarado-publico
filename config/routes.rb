Rails.application.routes.draw do
  get 'sessions/new'
  get 'sessions/create'
  get 'sessions/destroy'
  get 'user/index'
  get 'user/show'
  get 'user/new'
  get 'user/create'
  get 'user/destroy'
  resources :main_banners
  root 'main#home'

  get 'home', to: 'main#home', as: :home
  get 'privacy_policy', to: 'main#privacy_policy', as: :privacy_policy
  get 'terms_and_conditions', to: 'main#terms_and_conditions', as: :terms_and_conditions
  get 'frequent_questions', to: 'main#frequent_questions', as: :frequent_questions
  get 'who_we_are', to: 'main#who_we_are', as: :who_we_are
  get 'contact_us', to: 'main#contact_us', as: :contact_us
  get '/product/model_ea2', to: 'main#model_ea2', as: :model_ea2

  # log in page with form:
  get '/login'     => 'sessions#new', as: :login_form
  post '/login'    => 'sessions#create', as: :login
  get '/logout' => 'sessions#destroy', as: :logout

  get 'admin/dashboard', as: :dashboard
end
